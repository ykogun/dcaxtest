#!/bin/bash

#declare -rx CHEFURL="https://packagecloud.io/chef/stable/packages/el/6/chef-server-core-12.3.1-1.el6.x86_64.rpm/download"
declare -rx CHEFURL="https://packagecloud.io/chef/stable/packages/el/6/chef-server-core-12.3.1-1.el6.x86_64.rpm/download"
declare -rx SCRIPT=${0##*/}
declare -rx BASEDIR=$(pwd)
declare -rx CURL=/usr/bin/curl
declare -rx WGET=/usr/bin/wget
declare -rx URLCH=$(curl -o /dev/null --silent --head --write-out '%{http_code}\n' https://bitbucket.org )
declare -rx RPM=/bin/rpm
declare -rx CHFCTL=/usr/bin/chef-server-ctl
declare -rx INUSER=pcrafford
declare -rx FNAME=Peter
declare -rx LNAME=Crafford
declare -rx EMAIL=pcrafford@thunderhead.com
declare -rx PASSWORD=Smangomp2
declare -rx CHFMNG=/usr/bin/opscode-manage-ctl
declare -rx CHFPSH=/usr/bin/opscode-push-jobs-server-ctl
declare -rx CHFREP=/usr/bin/opscode-reporting-ctl
declare -rx ORG=$1
declare -rx HOME=/root
declare -rx IPTAB=/sbin/iptables
declare -rx SERV=/sbin/service
declare -rx SED=/bin/sed
declare -rx YUM=/usr/bin/yum

exec 4>"$HOME"/installinfo.log
exec 5>"$HOME"/installerr.log

if [ $# != 1 ] ; then
        printf "1 arguments needed:\n " >&4
        printf "Aborted, no envronmrnt listed, Please enter value for chef org. \n" >&4
        exit 10
fi

printf "$ORG \n" >&4


#Ensure wget is installed
if  test -f $WGET ; then
        printf "$SCRIPT:$LINENO: wget found at $WGET\n" >&4
else
        printf "$SCRIPT:$LINENO: $WGET not found exiting.......\n" >&4
        exit
fi

#ensure curl is installed
if test -f $CURL ; then
	printf "$SCRIPT:$LINENO: curl found at $CURL\n" >&4
else
	printf "$SCRIPT:$LINENO: curl not found exiting.....\n" >&4
	exit
fi

# Ensure there is a valid internet connection
if [ $URLCH = 200 ] ; then
	printf "Great internet connection working.....\n" >&4
else
	printf "No connection internet connection exiting......\n" >&4
	exit
fi

#$SED -i "1s/.*/127.0.0.1\  $(hostname)/" /etc/hosts
#$SED -i "2i127.0.0.1  localhost" /etc/hosts
#$SED -i "2i$(hostname -I)  $(hostname)" /etc/hosts

printf "Fetching the software from $CHEFURL \n" >&4

$WGET -N -O chef-servers $CHEFURL 2>&5

$RPM -Uvh chef-server* 2>&5

$CHFCTL reconfigure >&5

printf "installing Manager Interface.\n" >&4
$CHFCTL install opscode-manage >&5

printf "Run reconfigure for opscode-manage\n" >&4
$CHFCTL reconfigure >&5 2>&4

printf "reconfigure using opscode-manage-ctl\n" >&4
$CHFMNG reconfigure >&5 2>&4

#printf "Installng Push Server.\n" >&4
#$CHFCTL install opscode-push-jobs-server >&5 2>&4

#printf "Run reconfigure for opscode-push-jobs-server\n" >&4
#$CHFCTL reconfigure >&5 2>&4

#printf "reconfigure using using opscode-push-jobs-server-ctl\n" >&4
#$CHFPSH reconfigure >&5 2>&4

#printf "Installing Reporting Server.\n" >&4
#$CHFCTL install opscode-reporting >&5 2>&4

#printf "Run reconfigure for opscode-reporting\n" >&4
#$CHFCTL reconfigure >&5 2>&4

#printf "reconfigure using using opscode-reporting-ctl\n" >&4
#$CHFREP reconfigure >&5 2>&4

printf "creating initial admin user in $HOME\n" >&4
$CHFCTL user-create $INUSER $FNAME $LNAME $EMAIL $PASSWORD -f $HOME/"$INUSER".pem >&4
#$CHFCTL user-create $INUSER $FNAME $LNAME $EMAIL $PASSWORD >&4
$CHFCTL user-list >&4
printf "$ORG \n" >&4
printf "creating the Org\n" >&4
$CHFCTL org-create $ORG "$ORG" --association_user $INUSER -f $HOME/"$ORG"-validator.pem >&4
#$CHFCTL org-create $ORG "$ORG" --association_user $INUSER >&4 2>&4
printf "The org Created is:\n" >&4
$CHFCTL org-list >&4

printf "the following chef components are installed:\n" >&4
printf "##################################################\n" >&4
$CHFCTL service-list >&4
$CHFCTL status >&4

$IPTAB  -I INPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT
$IPTAB  -I INPUT -m state --state NEW -p tcp --dport 443 -j ACCEPT
$SERV iptables save
$SERV iptables restart
